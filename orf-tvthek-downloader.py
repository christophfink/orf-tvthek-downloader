#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  orf-tvthek-downloader.py
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import os, os.path, sys, re, urllib2, subprocess, json, platform
from HTMLParser import HTMLParser

def findVLC():
	if platform.system()=="Darwin":
		vlcCmd=which("/Applications/VLC.app/Contents/MacOS/VLC")
		if vlcCmd is not None:
			return vlcCmd
	if platform.system()=="Windows":
		try:
			import _winreg
			registry=_winreg.ConnectRegistry(None,_winreg.HKEY_LOCAL_MACHINE)
			vlcCmd=_winreg.QueryValue(registry, r'SOFTWARE\Wow6432Node\VideoLAN\VLC') 
			vlcCmd=which(vlcCmd)
			if vlcCmd is not None:
				return vlcCmd
		except:
			vlcCmd=which("vlc.exe")
			if vlcCmd is not None:
				return vlcCmd
	vlcCmd=which("vlc")
	if vlcCmd is not None:
		return vlcCmd

	return None
	

def which(program):
	def is_exe(fpath):
		return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

	fpath, fname = os.path.split(program)
	if fpath:
		if is_exe(program):
			return program
	else:
		for path in os.environ["PATH"].split(os.pathsep):
			path = path.strip('"')
			exe_file = os.path.join(path, program)
			if is_exe(exe_file):
				return exe_file
	return None


def main():
	if len(sys.argv)<2:
		print("USAGE: %s [destinationDir] videoPageUrl [videoPageUrl … ]" %(os.path.basename(__file__),))
		return

	videoPageUrls=[]
	dstDir=None
	for arg in sys.argv[1:]:
		if re.match('https?://([a-z0-9.\-]+\.)+[a-z]{2,}/([a-z0-9.\-]+/?)*',arg) is not None:
			videoPageUrls.append(arg)
		elif os.path.isdir(arg) and os.access(arg, os.W_OK) and dstDir is None:
			dstDir=os.path.abspath(arg)

	vlcCmd=findVLC()
	if vlcCmd is None:
		print("FATAL: Could not find local VLC executables.")
		return

	if dstDir is None:
		homeDir=os.path.expanduser("~")
		preferredSubDirs=["Videos","Downloads","Download",""]
		for subDir in preferredSubDirs:
			if os.path.isdir(os.path.join(homeDir,subDir)) and os.access(os.path.join(homeDir,subDir), os.W_OK):
				dstDir=os.path.join(homeDir,subDir)
				break
	#print(vlcCmd)
	#print(dstDir)
	#print(videoPageUrls)
	#return
	
	for videoPageUrl in videoPageUrls:
		try:
			f = urllib2.urlopen(videoPageUrl)
		except ValueError:  
			print("Invalid Argument")
			continue

		#with open("testfile","w") as wf:
		#	wf.write(f.read())
		#break
		
		for line in f.readlines():
			jsbData=False
			#if "jsb_PlaylistItem" in line:
			#	print("möp")
			#	jsbData=json.loads(
			#		HTMLParser().unescape(
			#			re.search(
			#				'data-jsb="(?P<jsbData>[^"]*)"',
			#				line
			#			).group('jsbData')
			#		)
			#	)

			if "jsb_VideoPlaylist" in line:
				jsbData=json.loads(
					HTMLParser().unescape(
						re.search(
							'data-jsb="(?P<jsbData>[^"]*)"',
							line
						).group('jsbData')
					)
				)

			if jsbData is not False:
				#print(json.dumps(jsbData, sort_keys=True, indent=4, separators=(',', ': ')))
				#continue
				
				baseFileName="".join([c for c in jsbData["playlist"]["title"] if c.isalpha() or c.isdigit() or c==' ']).rstrip().replace(" ","_")
				videoSourceUrls=[]
				for video in jsbData["playlist"]["videos"]:
					for source in video["sources"]:
						if "delivery" in source and "quality" in source and "src" in source:
							#print(source["quality"])
							if source["delivery"]=="progressive" and source["quality"]=="Q8C":
								videoSourceUrls.append(source["src"])
				#subtitleSourceUrls={}
				#if "subtitles" in jsbData["selected_video"]:
				#	for subtitleSource in jsbData["selected_video"]["subtitles"]:
				#		if subtitleSource["type"]=="srt":
				#			subtitleSourceUrls[baseFileName+"_"+subtitleSource["lang"]+".srt"]=subtitleSource["src"];

				#print(videoSourceUrls)
				#continue
				#print(subtitleSourceUrls)

				#print("Downloading '%s' to %s" %(jsbData["selected_video"]["title"],dstDir))

				#for subtitleSourceUrl in subtitleSourceUrls:
				#	u=urllib2.urlopen(subtitleSourceUrls[subtitleSourceUrl])
				#	with open(os.path.join(dstDir,subtitleSourceUrl),"w") as f:
				#		for line in u.readlines():
				#			f.write(line)
				
				print(str(videoSourceUrls))
				#for videoSourceUrl in videoSourceUrls:
				vlcCmdOptions=[
					vlcCmd,
                    "-v",
					"-I", "dummy",
                    "--sout","#gather:std{access=file,mux=mp4,dst="+os.path.join(dstDir,baseFileName)+".mp4}"
                ]
#                    "--sout-mux-caching","55000",
#					"--sout-avcodec-strict","-2",
#					"--sout-vorbis-quality","9",
#					"--sout","#transcode{vcodec=VP80,venc=ffmpeg{qmin=20,qmax=63},acodec=vorb,vb=2000}:std{access=file,mux=ogg,dst="+os.path.join(dstDir,baseFileName)+".ogv}",
#					"--sout-keep"
#				]
				#"--sout","#transcode{vcodec=mp4v,acodec=mp4a}:std{access=file,mux=mp4,dst="+os.path.join(dstDir,baseFileName)+"}",
				vlcCmdOptions.extend(videoSourceUrls)
				vlcCmdOptions.append("vlc://quit")
				if platform.system()=="Windows":
					startupinfo = subprocess.STARTUPINFO()
					startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
					subprocess.check_call(
						vlcCmdOptions,
						startupinfo=startupinfo
					)
				else:
					subprocess.check_call(
						vlcCmdOptions
					)



if __name__ == '__main__':
        main()
